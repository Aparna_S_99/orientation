## Docker  
* An open platform for developing, shipping, and running applications
* Enabling seperation of application from infrastructure for quick software delivery
## Why Docker?   
* Provides ability to package and run an application in a loosely isolated environment called a container
* Isolation and security to run many containers simultaneously on a given host 
* Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel, meaning more containers on a given hardware combination than virtual machines 
## Docker vs Virtual Machine  
![difference between docker and VM](/extras/Docker-vs-VM.png) 
## Docker Engine  
Docker engine is a clinet-server application with these major components:  
* A server which is a type of long-running program called a daemon process
* A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do
* A command line interface (CLI) client  
![](/extras/engine-components-flow.png)  
## Docker Architecture  
![](/extras/Docker-architecture.png)  
Docker uses a client-server architecture.  
Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing Docker containers  
Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface  
Docker registry stores Docker images  
## Docker Objects  
### Images  
contains everything needed to run an application as a container  
 * code  
 * runtime  
 * libraries
 * environment variables 
 * configuration files  
### Containers  
A container is a runnable instance of an image  
## Docker Commands  
| Command               | Description                                       |
|-----------------------|---------------------------------------------------|
| `docker ps`           |  to view all the containers that are running on the Docker Host|
| `docker start`        | starts any stopped container(s)                   |
| `docker stop`         | stops any running container(s)                    |
| `docker run`          | creates containers from docker images             |
| `docker rm`           | deletes the containers                            |
## Docker Process  
![](/extras/Docker-process.png)  
