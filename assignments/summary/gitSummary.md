## What is Git?  
Git is a distributed version-control system for tracking changes in source code during software development  
## Why Git?  
* lets you easily keep track of every revision you and your team make
* all share one repository of code that is worked on independently and then merged back together
* no need to be connected all the time because the project is both saved locally on each machine and remotely
* can to revert back to any previous version of your code, branch off and develop a specific feature without affecting anything or anyone else, and maintain safety from data corruption because of its distributed nature  
## Vocabulary  
1. **Repository**: collection of files and folders (code files) that you’re using git to track
2. **Github**:  popular remote storage solution for git repos
3. **Commit** : capturing a snapshot of the project's currently staged changes
4. **Push** : transfering commits from your local repository to a remote repo
5. **Branch**: separate instances of the code that offshoots from the main codebase
6. **Merge**:  integrating two branches together. Any new or updated code will become an official part of the codebase
7. **Clone** : making an exact copy of entire online repository on your local machine
8. **Fork**: you get an entirely new repo of code under your own name  
## Git Internals  
### States that files can reside in:
1. **Modify**: Changed the file but not committed to the repo
2. **Staged**: Marked the file to go into the next picture/snapshot
3. **Committed**: data is safely stored in your local repo in form of pictures/snapshots  
### Different trees of software code/repository:  
1. **Workspace**: Changes made via Editor is done in this tree
2. **Staging**: All staged files go into this tree
3. **Local Repository**: All committed files go into this tree
3. **Remote Repository**: copy of  Local Repository but is stored in some server on the Internet
## Git Workflow and Commands
![git workflow flowchart](/extras/git-workflow.jpg)
## Git Commands  
| Commands                         |  Description                                  |
|----------------------------------|-----------------------------------------------|
| `git init`                       | to start a new repository                     |
| `git clone`                      | to start working locally on an existing remote repository|
| `git add <file-name OR folder-name>`| adding files to staging area               |
| `git commit`                     | snapshots file in the current version         |
| `git checkout -b <name-of-branch>`| create a new branch, to work from without affecting the master branch|
| `git checkout <name-of-branch>`  | switch to the given branch                    |
| `git push`                       | to push all local commits (saved changes) to the remote repository|
| `git reset .`                    |  undo the most recently added, but not committed, changes to files/folders|
